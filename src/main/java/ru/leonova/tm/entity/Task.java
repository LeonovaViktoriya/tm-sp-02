package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.leonova.tm.enumerated.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_task")
public final class Task implements Serializable {

    @Id
    @Column(name = "id")
    private String taskId;
    private String name;
    private String description;
    @Column(name = "createDate")
    private Date dateSystem;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    @Column(name = "beginDate")
    private Date dateStart;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    @Column(name = "endDate")
    private Date dateEnd;
    @Column(name = "statusType")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

}

