package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.repository.IProjectRepository;

import java.util.List;


@Service
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @Transactional
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findOneById(@NotNull final String projectId) {
        return projectRepository.getOne(projectId);
    }

    @Override
    public void createProject(@NotNull final Project project) {
        projectRepository.save(project);
    }

    @Override
    public void save(@NotNull final Project project) {
        projectRepository.save(project);
    }

    @Override
    public void removeProject(@NotNull final Project project) {
        projectRepository.delete(project);
    }

    @Transactional
    @Override
    public void update(@NotNull final Project project) {
        if (project.getProjectId().isEmpty() || project.getName().isEmpty() || project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new IllegalArgumentException();
        }
        projectRepository.update(project.getProjectId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateEnd(), project.getStatus());
    }
}
