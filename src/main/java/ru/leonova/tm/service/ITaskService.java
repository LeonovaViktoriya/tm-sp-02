package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;

import java.util.List;

public interface ITaskService {
    List<Task> findAll();

    void create(@NotNull Task task);

    Task findOneById(@NotNull String id);

    void removeTask(@NotNull Task task);

    void update(@NotNull Task task);

    List<Task> findAllByProjectId(@NotNull String id);

    void removeAllByProjectId(@NotNull String id);
}
