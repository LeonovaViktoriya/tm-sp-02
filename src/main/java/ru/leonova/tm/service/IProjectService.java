package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Project;

import java.util.List;

public interface IProjectService {
    @Transactional
    List<Project> findAll();

    Project findOneById(@NotNull String projectId);

    void createProject(@NotNull Project project);

    void save(@NotNull Project project);

    void removeProject(@NotNull Project project);

    @Transactional
    void update(@NotNull Project project);
}
