package ru.leonova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {
    List<Task> findAllByProjectProjectId(String projectId);
    void deleteAllByProject_ProjectId(String projectId);
    @Modifying
    @Query("UPDATE Task SET name = :name, description = :description,dateStart = :dateStart, dateEnd = :dateEnd, status = :status WHERE taskId = :id")
    void update(@Param("id") final String id, @Param("name") final String name, @Param("description") final String description,
                @Param("dateStart") final Date dateStart, @Param("dateEnd") final Date dateEnd, @Param("status") final Status status);
}
