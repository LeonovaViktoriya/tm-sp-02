package ru.leonova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyUtil {

    @NotNull
    private final Properties properties = new Properties();

    public PropertyUtil() {
        try {
//            @NotNull String PATH_TO_PROPERTIES = "src/main/resources/mysql.properties";
            @NotNull String PATH_TO_PROPERTIES = "src/main/resources/postgres.properties";
            @NotNull final FileInputStream fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            properties.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public final String getUrl() {
        return properties.getProperty("spring.datasource.url");
    }

    @Nullable
    public final String getDriver() {
        return properties.getProperty("spring.datasource.driver-class-name");
    }

    @Nullable
    public final String getDbPort() {
        return properties.getProperty("server.port");
    }

    @Nullable
    public final String getHBM2DDL_AUTO() {
        return properties.getProperty("spring.jpa.hibernate.ddl-auto");
    }

    @Nullable
    public final String getShowSQL() {
        return properties.getProperty("spring.jpa.show-sql");
    }

    @Nullable
    public final String getDialect() {
        return properties.getProperty("spring.jpa.properties.hibernate.dialect");
    }

    @Nullable
    public final String getDbName() {
        return properties.getProperty("spring.application.name");
    }

    @Nullable
    public final String getUserName() {
        return properties.getProperty("spring.datasource.username");
    }

    @Nullable
    public final String getUserPassword() {
        return properties.getProperty("spring.datasource.password");
    }

}
